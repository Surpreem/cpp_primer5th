#include <iostream>

using std::cout;
using std::endl;


int odd[]{1, 3, 5, 7, 9};
int even[]{0, 2, 4, 6, 8};

decltype(odd)& arrRef(int i) {
  return (i % 2) ? odd : even;
}

int main() {
  int val = 3;
  decltype(odd)& arr = arrRef(val);
  for (auto i : arr) {
    cout << i << endl;
  }

  return 0;
}
