#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;


bool isUppercase(string const& str) {
  bool result = false;
  for (auto c : str) {
    if (isupper(c)) {
      result = true;
      break;
    }
  }
  return result;
}

void makeUppercase(string& str) {
  for (auto& c : str) {
    c = toupper(c);
  }
}

int main() {
  string str = "lowerCase";
  cout << "Before : " << str << endl;
  cout << "Has uppercase letter : "
      << (isUppercase(str) ? "yes" : "no") << endl;
  makeUppercase(str);
  cout << "After : " << str << endl;

  return 0;
}
