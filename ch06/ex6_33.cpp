#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;


void print_vector(vector<int> const& vec, vector<int>::size_type idx) {
  if (0 < idx) {
    print_vector(vec, idx - 1);
  }

  if (vec.size() > idx) {
    cout << vec[idx] << endl;
  }
  return;
}

int main() {
  vector<int> ivec{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  print_vector(ivec, ivec.size());

  return 0;
}
