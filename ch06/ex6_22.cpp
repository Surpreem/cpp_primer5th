#include <iostream>

using std::cout;
using std::endl;


void swapPtr(int*& p1, int*& p2) {
  int* temp = p1;
  p1 = p2;
  p2 = temp;
}

int main() {
  int val1 = 1;
  int val2 = 2;
  int* p1 = &val1;
  int* p2 = &val2;
  cout << "Before : p1 = " << p1 << ", p2 = " << p2 << endl;
  swapPtr(p1, p2);
  cout << "After : p1 = " << p1 << ", p2 = " << p2 << endl;

  return 0;
}
