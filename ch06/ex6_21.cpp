#include <iostream>

using std::cout;
using std::endl;


int largerValue(int val1, int const* val2) {
  return (val1 >= *val2) ? val1 : *val2;
}

int main() {
  int val1 = 3;
  int val2 = 5;
  cout << largerValue(val1, &val2) << endl;

  return 0;
}
