#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

int sum(int a, int b) {
  return a + b;
}

int sub(int a, int b) {
  return a - b;
}

int mul(int a, int b) {
  return a * b;
}

int divide(int a, int b) {
  return b ? a / b : 0;
}

int main() {
  using Func = int(int, int);
  vector<Func*> funcVec;

  funcVec.push_back(sum);
  funcVec.push_back(sub);
  funcVec.push_back(mul);
  funcVec.push_back(divide);

  cout << "sum: " << funcVec[0](4, 2) << endl;
  cout << "sub: " << funcVec[1](4, 2) << endl;
  cout << "mul: " << funcVec[2](4, 2) << endl;
  cout << "div: " << funcVec[3](4, 2) << endl;

  return 0;
}
