#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

using ArrT = string[10];

ArrT& func1(ArrT& arr) {
  arr[0] = "str 1";
  arr[1] = "str 2";
  arr[2] = "str 3";

  return arr;
}

auto func2(ArrT& arr) -> string(&)[10] {
  arr[0] = "str 1";
  arr[1] = "str 2";
  arr[2] = "str 3";

  return arr;
}

string arr_e[10];
decltype(arr_e)& func3(ArrT& arr) {
  arr[0] = "str 1";
  arr[1] = "str 2";
  arr[2] = "str 3";

  return arr;
}


int main() {
  ArrT arr_a;
  ArrT& arr_b = func1(arr_a);
  for (auto str : arr_b) {
    cout << str << endl;
  }

  ArrT arr_c;
  ArrT& arr_d = func2(arr_c);
  for (auto str : arr_d) {
    cout << str << endl;
  }

  ArrT& arr_f = func2(arr_e);
  for (auto str : arr_f) {
    cout << str << endl;
  }

  return 0;
}
