#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;


string (&func(string (&arr)[10]))[10] {
  arr[0] = "str 1";
  arr[1] = "str 2";
  arr[2] = "str 3";

  return arr;
}

int main() {
  string arr_a[10];
  string (&arr_b)[10] = func(arr_a);

  for (auto str : arr_b) {
    cout << str << endl;
  }
  return 0;
}
