#include <iostream>

using std::cin;
using std::cout;
using std::endl;

void fact(void) {
  int val = 0;
  cout << "Input value: ";
  cin >> val;

  cout << val << "! is ";
  int result = 1;
  while (1 < val) {
    result *= val--;
  }
  cout << result << endl;
}

int main() {
  fact();

  return 0;
}
