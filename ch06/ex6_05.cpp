#include <iostream>

using std::cout;
using std::endl;

int absolute(int val) {
  if (0 > val) {
    val = -val;
  }

  return val;
}

int main() {
  int j = absolute(-3);
  cout << "absolute(-3) is " << j << endl;

  return 0;
}
