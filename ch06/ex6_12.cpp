#include <iostream>

using std::cout;
using std::endl;


void swap(int& a, int& b) {
  int temp = a;
  a = b;
  b = temp;
}

int main() {
  int a = 2, b = 7;
  cout << "before: a = " << a << ", b = " << b << endl;
  swap(a, b);
  cout << "after: a = " << a << ", b = " << b << endl;

  return 0;
}
