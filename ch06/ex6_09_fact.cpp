#include "Chapter6.h"


int fact(int val) {
  int ret = 1;
  while (1 < val) {
    ret *= val--;
  }
  return ret;
}
