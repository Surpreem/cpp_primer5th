#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include <string>


struct Person {
  Person() = default;
  Person(std::string const& n)
      : name(n) { }
  Person(std::string const& n, std::string const& a)
      : name(n), address(a) { }
  Person(std::istream& is);

  std::string getName() const {return name;}
  std::string getAddress() const {return address;}

  std::string name;
  std::string address;
};

std::istream& read(std::istream& is, Person& person);
std::ostream& print(std::ostream& os, Person const& person);

#endif // PERSON_H
