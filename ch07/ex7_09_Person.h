#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include <string>


struct Person {
  std::string getName() const {return name;}
  std::string getAddress() const {return address;}

  std::string name;
  std::string address;
};

std::istream& read(std::istream& is, Person& person);
std::ostream& print(std::ostream& os, Person const& person);

#endif // PERSON_H
