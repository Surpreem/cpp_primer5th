#include "Sales_data.h"

#include <iostream>

using std::cin;
using std::cerr;
using std::cout;
using std::endl;


int main() {
  Sales_data trans1;
  print(cout, trans1) << endl;

  Sales_data trans2("0-201-78345-X");
  print(cout, trans2) << endl;

  Sales_data trans3("0-201-78345-X", 3, 20.00);
  print(cout, trans3) << endl;

  Sales_data trans4(cin);
  print(cout, trans4) << endl;

  return 0;
}
