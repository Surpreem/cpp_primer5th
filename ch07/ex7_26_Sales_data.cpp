#include "Sales_data.h"

using std::istream;
using std::ostream;


Sales_data::Sales_data(std::istream& is) {
  read(is, *this);
}

Sales_data& Sales_data::combine(Sales_data const& rhs) {
  units_sold += rhs.units_sold;
  revenue += rhs.revenue;
  return *this;
}

Sales_data add(Sales_data const& lhs, Sales_data const& rhs) {
  Sales_data sum = lhs;
  sum.combine(lhs);
  return sum;
}

istream& read(istream& is, Sales_data& item) {
  double price = 0.0;
  is >> item.bookNo >> item.units_sold >> price;
  item.revenue = price * item.units_sold;
  return is;
}

ostream& print(ostream& os, Sales_data const& item) {
  os << item.isbn() << " " << item.units_sold << " "
      << item.revenue << " " << item.avg_price();
  return os;
}
