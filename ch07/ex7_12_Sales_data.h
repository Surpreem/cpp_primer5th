#ifndef SALES_DATA_H
#define SALES_DATA_H

#include <iostream>
#include <string>


struct Sales_data {
  Sales_data() = default;
  Sales_data(std::string const& s) : bookNo(s) { }
  Sales_data(std::string const& s, unsigned n, double p)
      : bookNo(s), units_sold(n), revenue(p * n) { }
  Sales_data(std::istream& is) { read(is, *this); }

  std::string isbn() const {return bookNo;}
  Sales_data& combine(Sales_data const&);
  double avg_price() const;

  std::string bookNo;
  unsigned units_sold = 0;
  double revenue = 0.0;
};

Sales_data add(Sales_data const& lhs, Sales_data const& rhs);
std::istream& read(std::istream& is, Sales_data& item);
std::ostream& print(std::ostream& os, Sales_data const& item);

#endif // SALES_DATA_H
