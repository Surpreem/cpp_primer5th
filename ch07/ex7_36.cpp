#include <iostream>

using std::cout;
using std::endl;


struct X {
  X(int i, int j) : base(i), rem(base % j) { }
  int base, rem;
};

int main() {
  X x{4, 2};
  cout << "x.base = " << x.base << ", " << "x.rem = " << x.rem << endl;
}
