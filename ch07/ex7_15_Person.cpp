#include "Person.h"


using std::endl;
using std::istream;
using std::ostream;

Person::Person(std::istream& is) {
  read(is, *this);
}

istream& read(istream& is, Person& person) {
  is >> person.name >> person.address;
  return is;
}

ostream& print(ostream& os, Person const& person) {
  os << person.getName() << " " << person.getAddress();
  return os;
}
