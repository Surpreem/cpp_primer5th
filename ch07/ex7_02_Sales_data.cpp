#include "Sales_data.h"


Sales_data& Sales_data::combine(Sales_data const& rhs) {
  units_sold += rhs.units_sold;
  revenue += rhs.revenue;
  return *this;
}
