#include <iostream>
#include <stdexcept>

using std::cin;
using std::cout;
using std::endl;
using std::domain_error;


int main() {
  int val1 = 0;
  int val2 = 0;

  cout << "Input two integers: ";
  cin >> val1 >> val2;
  if (0 == val2) {
    throw domain_error("The second value must not be 0!");
  }
  cout << val1 << " / " << val2 << " = " << val1 / val2 << endl;

  return 0;
}
