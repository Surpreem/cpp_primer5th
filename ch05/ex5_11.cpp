#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;


int main() {
  string inputLine;
  unsigned aCnt = 0;
  unsigned eCnt = 0;
  unsigned iCnt = 0;
  unsigned oCnt = 0;
  unsigned uCnt = 0;
  unsigned spaceCnt = 0;
  unsigned tabCnt = 0;
  unsigned newLineCnt = 0;

  while (getline(cin, inputLine)) {
    for (char ch : inputLine) {
      switch (ch) {
        case 'a': case 'A':
          ++aCnt;
          break;

        case 'e': case 'E':
          ++eCnt;
          break;

        case 'i': case 'I':
          ++iCnt;
          break;

        case 'o': case 'O':
          ++oCnt;
          break;

        case 'u': case 'U':
          ++uCnt;
          break;

        case ' ':
          ++spaceCnt;
          break;

        case '\t':
          ++tabCnt;
          break;
      }
    }
    ++newLineCnt;
  }

  cout << "Number of vowel a: \t" << aCnt << '\n'
      <<  "Number of vowel e: \t" << eCnt << '\n'
      <<  "Number of vowel i: \t" << iCnt << '\n'
      <<  "Number of vowel o: \t" << oCnt << '\n'
      <<  "Number of vowel u: \t" << uCnt << '\n'
      <<  "Number of space: \t" << spaceCnt << '\n'
      <<  "Number of tab char: \t" << tabCnt << '\n'
      <<  "Number of new line: \t" << newLineCnt << endl;

  return 0;
}
