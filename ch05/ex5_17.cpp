#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;


int main() {
  vector<int> vec1{0, 1, 1, 2};
  vector<int> vec2{0, 1, 1, 2, 3, 5, 8};

  decltype(vec1.size()) const size =
      (vec1.size() >= vec2.size()) ? vec2.size() : vec1.size();
  for (decltype(vec1.size()) idx = 0; size != idx; ++idx) {
    if (vec1[idx] != vec2[idx]) {
      cout << "Result is false" << endl;
      return false;
    }
  }

  cout << "Result is true" << endl;
  return true;
}
