#include <iostream>

using std::cin;
using std::cout;
using std::endl;


int main() {
  int val1 = 0;
  int val2 = 0;

  cout << "Input two integers: ";
  cin >> val1 >> val2;
  cout << val1 << " / " << val2 << " = " << val1 / val2 << endl;

  return 0;
}
