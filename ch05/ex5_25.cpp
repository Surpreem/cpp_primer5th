#include <iostream>
#include <stdexcept>

using std::cin;
using std::cout;
using std::endl;
using std::domain_error;


int main() {
  int val1 = 0;
  int val2 = 0;

  cout << "Input two integers: ";
  while (cin >> val1 >> val2) {
    try {
      if (0 == val2) {
        throw domain_error("The second value must not be 0!");
      }
    } catch (domain_error err) {
      cout << err.what() << '\n'
          << "Try again.\nInput two integers: ";
      continue;
    }
    cout << val1 << " / " << val2 << " = " << val1 / val2 << endl;
    cout << "Input two integers: ";
  }

  return 0;
}
