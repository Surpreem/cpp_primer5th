#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;


int main() {
  string input;
  string previous;
  while (cin >> input) {
    if (isupper(previous[0]) && previous == input) {
      break;
    } else {
      previous = input;
    }
  }

  if (cin) {
    cout << "successive word: " << input << endl;
  } else {
    cout << "successive word is none" << endl;
  }

  return 0;
}
