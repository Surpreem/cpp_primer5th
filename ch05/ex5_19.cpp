#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;


int main() {
  string response;
  do {
    cout << "Input two strings: ";
    string str1, str2;
    cin >> str1 >> str2;
    cout << ((str1 <= str2) ? str1 : str2)
        << " is smaller than the other." << "\n"
        << "More? Enter yes or no: ";
    cin >> response;
  } while (!response.empty() && 'n' != response[0]);

  return 0;
}
