#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;


int main() {
  const vector<string> scores{"F", "D", "C", "B", "A", "A++"};

  int grade{0};
  while (cin >> grade) {
    string lettergrade;
    if (60 > grade) {
      lettergrade = scores[0];
    } else {
      lettergrade = scores[(grade - 50) / 10];
      if (100 != grade) {
        if (7 < grade % 10) {
          lettergrade += '+';
        } else if (3 > grade % 10) {
          lettergrade += '-';
        }
      }
    }
    cout << lettergrade << endl;
  }

  return 0;
}
