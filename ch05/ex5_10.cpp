#include <iostream>

using std::cin;
using std::cout;
using std::endl;


int main() {
  char inputChar;
  unsigned aCnt = 0;
  unsigned eCnt = 0;
  unsigned iCnt = 0;
  unsigned oCnt = 0;
  unsigned uCnt = 0;

  while (cin >> inputChar) {
    switch (inputChar) {
      case 'a': case 'A':
        ++aCnt;
        break;

      case 'e': case 'E':
        ++eCnt;
        break;

      case 'i': case 'I':
        ++iCnt;
        break;

      case 'o': case 'O':
        ++oCnt;
        break;

      case 'u': case 'U':
        ++uCnt;
        break;
    }
  }

  cout << "Number of vowel a: \t" << aCnt << '\n'
      <<  "Number of vowel e: \t" << eCnt << '\n'
      <<  "Number of vowel i: \t" << iCnt << '\n'
      <<  "Number of vowel o: \t" << oCnt << '\n'
      <<  "Number of vowel u: \t" << uCnt << endl;

  return 0;
}
