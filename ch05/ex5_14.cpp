#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;


int main() {
  string read;
  string duplicated;
  string maxDuplicated;
  unsigned duplicatedCnt = 0;
  unsigned maxDuplicatedCnt = 0;

  while (cin >> read) {
    if (duplicated != read) {
      duplicated = read;
      duplicatedCnt = 1;
    } else {
      ++duplicatedCnt;
    }
    if (maxDuplicatedCnt < duplicatedCnt) {
      maxDuplicatedCnt = duplicatedCnt;
      maxDuplicated = duplicated;
    }
  }

  if (2 <= maxDuplicatedCnt) {
    cout << maxDuplicated << " is duplicated "
        << maxDuplicatedCnt << " times" << endl;
  } else {
    cout << "There's no duplicated string." << endl;
  }

  return 0;
}
