#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;


using Vec = vector<int>;

Vec::iterator find_value(Vec::iterator cbeg, Vec::iterator cend, int value) {
  while (cend != cbeg) {
    if (*cbeg == value) {
      break;
    }
    ++cbeg;
  }

  return cbeg;
}

int main() {
  Vec vec{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  Vec::iterator iter = find_value(vec.begin(), vec.end(), 5);
  cout << *iter << endl;

  return EXIT_SUCCESS;
}
