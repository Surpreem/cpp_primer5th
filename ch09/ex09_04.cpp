#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;


using Vec = vector<int>;

bool find_value(
    Vec::const_iterator cbeg, Vec::const_iterator cend, int value) {
      
  while (cend != cbeg) {
    if (*cbeg == value) {
      return true;
    }
    ++cbeg;
  }

  return false;
}

int main() {
  Vec vec{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  cout << find_value(vec.begin(), vec.end(), 5) << endl;
  cout << find_value(vec.begin(), vec.end(), 12) << endl;

  return EXIT_SUCCESS;
}
