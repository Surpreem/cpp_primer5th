#include <iostream>

int main() {
  int val = 10;

  for (int count = 0; count <= 10; ++count) {
    std::cout << val << std::endl;
    --val;
  }

  return 0;
}
