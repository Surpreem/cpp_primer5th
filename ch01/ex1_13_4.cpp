#include <iostream>

int main() {
  std::cout << "Enter two numbers:" << std::endl;

  int val1 = 0;
  int val2 = 0;
  std::cin >> val1 >> val2;

  std::cout << "The numbers from " << val1 << " "
            << "to " << val2 << " are ";

  for (; val1 <= val2; ++val1)
    std::cout << val1 << " ";

  std::cout << std::endl;

  return 0;
}
