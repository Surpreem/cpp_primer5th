#include <iostream>

int main() {
  int count = 0;
  int val = 10;

  while (count <= 10) {
    std::cout << val << std::endl;
    --val;
    ++count;
  }

  return 0;
}
