#include <iostream>
#include "Sales_item.h"


int main() {
  Sales_item book;
  Sales_item nextBook;
  int count = 0;

  if (std::cin >> book) {
    ++count;

    while (std::cin >> nextBook) {
      if (book.isbn() == nextBook.isbn()) {
        ++count;
      } else {
        std::cout << book.isbn() << ": " << count << std::endl;
        book = nextBook;
        count = 1;
      }
    }
    std::cout << book.isbn() << ": " << count << std::endl;
  } else {
    std::cout << "No data." << std::endl;
    return -1;
  }

  return 0;
}
