#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;


int main() {
  string str1;
  string str2;

  while (cin >> str1 >> str2) {
    if (str1 == str2) {
      cout << str1 << " equals to " << str2 << endl;
      cout << str1 << " is the same length with " << str2 << endl;
    } else if (str1 > str2) {
      cout << str1 << " is greater than " << str2 << endl;
    } else {
      cout << str2 << " is greater than " << str1 << endl;
    }

    string::size_type strSize1 = str1.size();
    string::size_type strSize2 = str2.size();
    if (strSize1 == strSize2) {
      cout << str1 << " is the same length with " << str2 << endl;
    } else if (strSize1 > strSize2) {
      cout << str1 << " is longer than " << str2 << endl;
    } else {
      cout << str2 << " is longer than " << str1 << endl;
    }
  }

  return 0;
}
