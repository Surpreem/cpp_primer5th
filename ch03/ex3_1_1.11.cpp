#include <iostream>

using std::cin;
using std::cout;
using std::endl;


int main() {
  cout << "Enter two numbers:" << endl;

  int val1 = 0;
  int val2 = 0;
  cin >> val1 >> val2;

  cout << "The numbers from " << val1 << " "
       << "to " << val2 << " are ";
  while (val1 <= val2) {
    cout << val1 << " ";
    ++val1;
  }
  cout << endl;

  return 0;
}
