#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;


int main() {
  string str1("A string example");
  string str2("A string example");

  if (str1 != str2) {
    cout << "str1 doesn't equal to str2." << endl;
  } else {
    cout << "str1 equals to str2." << endl;
  }

  return 0;
}
