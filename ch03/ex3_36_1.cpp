#include <iostream>
#include <iterator>

using std::begin;
using std::cout;
using std::endl;
using std::end;


int main() {
  int ia1[] = {0, 1, 2};
  int ia2[] = {1, 2};

  auto ia1_size = end(ia1) - begin(ia1);
  auto ia2_size = end(ia2) - begin(ia2);
  if (ia1_size != ia2_size) {
    cout << "ia1 doesn't equal to ia2." << endl;
    return -1;
  }

  for (decltype(ia1_size) idx = 0; ia1_size != idx; ++idx) {
    if (ia1[idx] != ia2[idx]) {
      cout << "ia1 doesn't equal to ia2." << endl;
      return -1;
    }
  }

  cout << "ia1 equals to ia2." << endl;

  return 0;
}
