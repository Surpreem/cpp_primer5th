#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;


int main() {
  string input;
  vector<string> vec;
  while (cin >> input) {
    vec.push_back(input);
  }

  for (auto output : vec) {
    cout << output << " ";
  }
  cout << endl;

  return 0;
}
