#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;


int main() {
  vector<int> ivec1(10, 42);
  vector<int> ivec2{42, 42, 42, 42, 42, 42, 42, 42, 42, 42};
  vector<int> ivec3;
  for (int idx = 0; 10 != idx; ++idx) {
    ivec3.push_back(42);
  }

  for (auto elem : ivec1) {
    cout << elem << " ";
  }
  cout << endl;

  for (auto elem : ivec2) {
    cout << elem << " ";
  }
  cout << endl;

  for (auto elem : ivec3) {
    cout << elem << " ";
  }
  cout << endl;

  return 0;
}
