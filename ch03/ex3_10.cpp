#include <iostream>
#include <cctype>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::getline;
using std::ispunct;


int main() {
  string str;
  while (getline(cin, str)) {
    for (auto c : str) {
      if (!std::ispunct(c)) {
        cout << c;
      }
    }
    cout << endl;
  }

  return 0;
}
