#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;


int main() {
  string str;
  if (cin >> str) {
    for (decltype(str.size()) idx = 0; str.size() != idx; ++idx) {
      str[idx] = 'X';
    }

    cout << str << endl;
  }

  return 0;
}
