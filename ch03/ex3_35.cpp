#include <iostream>
#include <iterator>
#include <vector>

using std::begin;
using std::cout;
using std::endl;
using std::end;
using std::vector;


int main() {
  constexpr size_t array_size = 10;
  int ia[array_size];
  for (size_t idx = 0; array_size != idx; ++idx) {
    ia[idx] = idx;
  }

  for (auto elem : ia) {
    cout << elem << " ";
  }
  cout << endl;

  for (auto ip = begin(ia); end(ia) != ip; ++ip) {
    *ip = 0;
  }

  for (auto elem : ia) {
    cout << elem << " ";
  }
  cout << endl;

  return 0;
}
