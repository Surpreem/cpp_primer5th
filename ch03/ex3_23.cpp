#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;


int main() {
  vector<int> ivec;
  for (int i = 1; 10 >= i; ++i) {
    ivec.push_back(i);
  }
  for (auto elem : ivec) {
    cout << elem << " ";
  }
  cout << endl;

  for (auto it = ivec.begin(); ivec.end() != it; ++it) {
    *it *= 2;
  }

  for (auto elem : ivec) {
    cout << elem << " ";
  }
  cout << endl;

  return 0;
}
