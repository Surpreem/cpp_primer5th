#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;


int main() {
  string strTotal;
  string strTotalWithSpace;
  string str;

  while (cin >> str) {
    strTotal += str;
    strTotalWithSpace += str;
    strTotalWithSpace += " ";
  }

  cout << strTotal << endl;
  cout << strTotalWithSpace << endl;

  return 0;
}
