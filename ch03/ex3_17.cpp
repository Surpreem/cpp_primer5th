#include <cctype>
#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;


int main() {
  string input;
  vector<string> vec;
  while (cin >> input) {
    vec.push_back(input);
  }

  int count = 1;
  for (auto str : vec) {
    for (auto& output : str) {
      output = toupper(output);
    }
    cout << str;
    if (count % 8) {
      cout << " ";
    } else {
      cout << endl;
    }
    ++count;
  }
  cout << endl;

  return 0;
}
