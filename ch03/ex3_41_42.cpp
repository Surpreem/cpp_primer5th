#include <iostream>
#include <iterator>
#include <vector>

using std::begin;
using std::cout;
using std::end;
using std::endl;
using std::vector;


int main() {
  int ia[] = {0, 1, 2, 3, 4, 5};
  vector<int> ivec(begin(ia), end(ia));

  for (auto elem : ivec) {
    cout << elem << " ";
  }
  cout << endl;

  int ia2[6] = {};
  for (decltype(ivec.size()) idx = 0; ivec.size() != idx; ++idx) {
    ia2[idx] = ivec[idx];
  }
  for (auto elem : ia2) {
    cout << elem << " ";
  }
  cout << endl;

  return 0;
}
