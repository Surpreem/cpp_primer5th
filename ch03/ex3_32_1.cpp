#include <iostream>

using std::cout;
using std::endl;


int main() {
  constexpr size_t array_size = 10;
  int ia[array_size];
  for (size_t idx = 0; array_size != idx; ++idx) {
    ia[idx] = idx;
  }

  for (auto elem : ia) {
    cout << elem << " ";
  }
  cout << endl;

  int ia2[array_size];
  for (size_t idx = 0; array_size != idx; ++idx) {
    ia2[idx] = ia[idx];
  }

  for (auto elem : ia2) {
    cout << elem << " ";
  }
  cout << endl;

  return 0;
}
