#include <cstring>
#include <iostream>

using std::cout;
using std::endl;


int main() {
  constexpr char ca1[] = "A string example";
  constexpr char ca2[] = "A string example";

  if (strcmp(ca1, ca2)) {
    cout << "ca1 doesn't equal to ca2." << endl;
  } else {
    cout << "ca1 equals to ca2." << endl;
  }

  return 0;
}
