#include <iostream>

using std::cout;
using std::endl;


int main() {
  int count = 0;
  int val = 10;

  while (count <= 10) {
    cout << val << endl;
    --val;
    ++count;
  }

  return 0;
}
