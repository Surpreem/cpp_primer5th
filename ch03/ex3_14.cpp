#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;


int main() {
  int input = 0;
  vector<int> vec;
  while (cin >> input) {
    vec.push_back(input);
  }

  for (auto output : vec) {
    cout << output << " ";
  }
  cout << endl;

  return 0;
}
