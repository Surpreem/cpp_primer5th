#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;


int main() {
  string str;
  if (cin >> str) {
    decltype(str.size()) idx = 0;
    while (str.size() != idx) {
      str[idx] = 'X';
      ++idx;
    }

    cout << str << endl;
  }

  return 0;
}
