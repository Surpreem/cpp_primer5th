#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;


int main() {
  vector<string> text{
    "This is a first sentence.",
    "This is a second sentence.",
    "",
    "This is a third sentence."};

  for (auto it = text.begin(); text.end() != it && !it->empty(); ++it) {
    for (auto& ch : *it) {
      ch = toupper(ch);
    }
    cout << *it << " ";
  }
  cout << endl;

  return 0;
}
