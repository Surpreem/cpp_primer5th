#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;


int main() {
  vector<int> ivec1(10);
  for (decltype(ivec1.size()) idx = 0; ivec1.size() != idx; ++idx) {
    ivec1[idx] = idx;
  }

  for (auto elem : ivec1) {
    cout << elem << " ";
  }
  cout << endl;

  vector<int> ivec2(ivec1);
  for (auto elem : ivec2) {
    cout << elem << " ";
  }
  cout << endl;

  return 0;
}
