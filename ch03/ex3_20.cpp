#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;


int main() {
  int input;
  vector<int> ivec;
  while (cin >> input) {
    ivec.push_back(input);
  }

  for (decltype(ivec.size()) idx = 0; ivec.size() - 1 != idx; ++idx) {
    cout << ivec[idx] + ivec[idx + 1] << " ";
  }
  cout << endl;

  decltype(ivec.size()) limit = ivec.size() / 2;
  if (ivec.size() % 2) {
    ++limit;
  }

  for (decltype(ivec.size()) idx = 0; limit != idx; ++idx) {
    cout << ivec[idx] + ivec[ivec.size() - idx - 1] << " ";
  }

  return 0;
}
