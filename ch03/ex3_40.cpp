#include <cstring>
#include <iostream>

using std::cout;
using std::endl;


int main() {
  constexpr char ca1[] = "The first sentence.";
  constexpr char ca2[] = "The second sentence.";
  char ca3[40] = {};

  strcpy(ca3, ca1);
  strcat(ca3, ca2);
  cout << ca3 << endl;

  return 0;
}
