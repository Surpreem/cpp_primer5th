#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;


int main() {
  vector<int> ivec1{0, 1, 2};
  vector<int> ivec2{0, 1};

  if (ivec1 != ivec2) {
    cout << "ivec1 doesn't equal to ivec2." << endl;
  } else {
    cout << "ivec1 equals to ivec2." << endl;
  }

  return 0;
}
