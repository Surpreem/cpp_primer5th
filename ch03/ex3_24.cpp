#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;


int main() {
  int input;
  vector<int> ivec;
  while (cin >> input) {
    ivec.push_back(input);
  }

  for (auto it = ivec.begin(); ivec.end() - 1 != it; ++it) {
    cout << *it + *(it + 1) << " ";
  }
  cout << endl;

  auto beg = ivec.begin();
  auto end = ivec.end();
  auto limit = (end - beg) / 2;
  if ((end - beg) % 2) {
    ++limit;
  }

  for (auto it = beg; beg + limit != it; ++it) {
    auto pos = end - it - 1;
    cout << *it + *(beg + pos) << " ";
  }

  return 0;
}
