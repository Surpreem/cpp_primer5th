#include <iostream>
#include <iterator>
#include <vector>

using std::begin;
using std::cout;
using std::end;
using std::endl;
using std::vector;


int main() {
  constexpr size_t rowCnt = 3;
  constexpr size_t colCnt = 4;
  int ia[rowCnt][colCnt] = {
    {0, 1, 2, 3},
    {4, 5, 6, 7},
    {8, 9, 10, 11}
  };

  for (auto const& p : ia) {
    for (auto const q : p) {
      cout << q << " ";
    }
    cout << endl;
  }
  cout << endl;

  for (size_t p = 0; rowCnt != p; ++p) {
    for (size_t q = 0; colCnt != q; ++q) {
      cout << ia[p][q] << " ";
    }
    cout << endl;
  }
  cout << endl;

  for (auto p = begin(ia); end(ia) != p; ++p) {
    for (auto q = begin(*p); end(*p) != q; ++q) {
      cout << *q << " ";
    }
    cout << endl;
  }
  cout << endl;

  return 0;
}
