#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::istream;
using std::string;


istream& print_input(istream& is) {
  string str;
  while (getline(is, str)) {
    cout << str;
  }
  is.clear();
  return is;
}


int main() {
  print_input(cin);
  return 0;
}
