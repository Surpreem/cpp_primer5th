#include <iostream>
#include <sstream>
#include <string>

using std::cin;
using std::cout;
using std::istream;
using std::istringstream;
using std::string;


istream& print_input(istream& is) {
  string str;
  while (getline(is, str)) {
    cout << str;
  }
  is.clear();
  return is;
}


int main() {
  istringstream line("istringstream line");
  print_input(line);
  return 0;
}
