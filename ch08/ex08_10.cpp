#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::ifstream;
using std::istringstream;
using std::string;
using std::vector;


int main(int argc, char* argv[]) {
  ifstream input(argv[1]);
  if (!input) {
    return EXIT_FAILURE;
  }

  vector<string> vec;
  string line;
  while (getline(input, line)) {
    vec.push_back(line);
  }

  for (auto str : vec) {
    istringstream record(str);
    string word;
    while (record >> word) {
      cout << word << endl;
    }
  }

  return EXIT_SUCCESS;
}
