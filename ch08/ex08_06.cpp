#include <fstream>
#include <iostream>
#include <string>


struct Sales_data {
  std::string bookNo;
  unsigned units_sold = 0;
  double revenue = 0.0;
};


int main(int argc, char* argv[]) {
  std::ifstream input(argv[1]);
  if (!input) {
    return -1;
  }

  double price = 0.0;
  Sales_data total;
  if (input >> total.bookNo >> total.units_sold >> price) {
    total.revenue = total.units_sold * price;

    Sales_data trans;
    while (input >> trans.bookNo >> trans.units_sold >> price) {
      trans.revenue = trans.units_sold * price;

      if (total.bookNo == trans.bookNo) {
        total.units_sold += trans.units_sold;
        total.revenue += trans.revenue;
      } else {
        std::cout << total.bookNo << " "
            << total.units_sold << " "
            << total.revenue << " "
            << total.revenue / total.units_sold << std::endl;

        total = trans;
      }
    }
    std::cout << total.bookNo << " "
        << total.units_sold << " "
        << total.revenue << " "
        << total.revenue / total.units_sold << std::endl;
  } else {
    std::cerr << "No data?!" << std::endl;
    return -1;
  }

  return 0;
}
