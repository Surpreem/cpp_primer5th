#include <fstream>
#include <string>
#include <vector>

using std::ifstream;
using std::istream;
using std::string;
using std::vector;


using Vec = vector<string>;

istream& read(istream& is, Vec& vec) {
  string str;
  while (getline(is, str)) {
    vec.push_back(str);
  }
  is.clear();
  return is;
}


int main(int argc, char* argv[]) {
  ifstream input(argv[1]);
  Vec vec;
  if (input) {
    read(input, vec);
  }

  return 0;
}
