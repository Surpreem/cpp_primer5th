#include <iostream>

using std::cin;
using std::cout;
using std::endl;


int main() {
  int value;
  cin >> value;

  cout << value;
  if (value % 2) {
    cout << " is a odd." << endl;
  } else {
    cout << " is a even." << endl;
  }

  return 0;
}
