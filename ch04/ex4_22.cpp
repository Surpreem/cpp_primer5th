#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;


int main() {
  vector<int> grades{40, 60, 75, 90, 91, 100, 76, 59};

  for (auto grade : grades) {
    cout << grade << " : " <<
        ((90 < grade) ? "high pass"
            : (60 <= grade && 75 >= grade) ? "low pass"
            : (60 > grade) ? "fail" : "pass")
        << endl;
  }
  cout << endl;

  string gradeResult;
  for (auto grade : grades) {
    if (90 < grade) {
      gradeResult = "high pass";
    }
    if (75 < grade && 90 >= grade) {
      gradeResult = "pass";
    }
    if (60 <= grade && 75 >= grade) {
      gradeResult = "low pass";
    }
    if (60 > grade) {
      gradeResult = "fail";
    }

    cout << grade << " : " << gradeResult << endl;
  }
  cout << endl;

  for (auto grade : grades) {
    if (90 < grade) {
      gradeResult = "high pass";
    } else {
      if (60 <= grade && 75 >= grade) {
        gradeResult = "low pass";
      } else {
        if (60 > grade) {
          gradeResult = "fail";
        } else {
          gradeResult = "pass";
        }
      }
    }

    cout << grade << " : " << gradeResult << endl;
  }
  cout << endl;

  return 0;
}
