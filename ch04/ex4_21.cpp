#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;


int main() {
  vector<int> ivec{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  for (auto elem : ivec) {
    cout << elem << " ";
  }
  cout << endl;

  for (auto& elem : ivec) {
    if (elem % 2) {
      elem *= 2;
    }
  }

  for (auto elem : ivec) {
    cout << elem << " ";
  }
  cout << endl;

  return 0;
}
