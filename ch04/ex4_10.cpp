#include <iostream>

using std::cin;
using std::cout;
using std::endl;


int main() {
  int value;

  cin >> value;
  while (42 != value) {
    cout << "Input value is " << value << endl;
    cin >> value;
  }

  return 0;
}
