#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;


int main() {
  vector<int> ivec(10, 0);
  vector<int>::size_type cnt = ivec.size();
  for (vector<int>::size_type ix = 0; ix != ivec.size(); ix++, cnt--) {
    ivec[ix] = cnt;
  }
  for (auto elem : ivec) {
    cout << elem << " ";
  }
  cout << endl;

  return 0;
}
