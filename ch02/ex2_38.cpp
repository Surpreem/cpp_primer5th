#include <iostream>


int main() {
  int a = 3, b = 4;

  auto c = a;
  decltype(a) d = a;

  auto e = b;
  decltype((b)) f = b;

  return 0;
}
