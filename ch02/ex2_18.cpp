#include <iostream>


int main() {
  int* p1 = 0;
  std::cout << p1 << std::endl;

  int i = 1024;
  p1 = &i;
  std::cout << p1 << " " << *p1 << std::endl;

  *p1 = 100;
  std::cout << p1 << " " << *p1 << std::endl;

  return 0;
}
