#include <iostream>
#include <string>


struct Sales_data {
  std::string bookNo;
  unsigned units_sold = 0;
  double revenue = 0.0;
};


int main() {
  Sales_data book1;
  Sales_data book2;
  double price = 0;

  std::cin >> book1.bookNo >> book1.units_sold >> price;
  book1.revenue = book1.units_sold * price;

  std::cin >> book2.bookNo >> book2.units_sold >> price;
  book2.revenue = book2.units_sold * price;

  if (book1.bookNo == book2.bookNo) {
    unsigned totalCnt = book1.units_sold + book2.units_sold;
    double totalRevenue = book1.revenue + book2.revenue;

    std::cout << book1.bookNo << " " << totalCnt << " " << totalRevenue << " ";
    if (0 != totalCnt) {
      std::cout << totalRevenue / totalCnt << std::endl;
    } else {
      std::cout << "No sales" << std::endl;
    }

    return 0;
  } else {
    std::cout << "Data must refer to the same ISBN" << std::endl;
    return -1;
  }

  return 0;
}
