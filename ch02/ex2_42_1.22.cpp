#include <iostream>
#include "Sales_data.h"


int main() {
  Sales_data book;
  Sales_data sum;
  double price = 0;

  while (std::cin >> book.bookNo >> book.units_sold >> price) {
    book.revenue = book.units_sold * price;

    sum.bookNo = book.bookNo;
    sum.units_sold += book.units_sold;
    sum.revenue += book.revenue;
  }

  std::cout << sum.bookNo << " "
    << sum.units_sold << " "
    << sum.revenue << " "
    << sum.revenue / sum.units_sold << std::endl;

  return 0;
}
