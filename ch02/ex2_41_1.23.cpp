#include <iostream>
#include <string>


struct Sales_data {
  std::string bookNo;
  unsigned units_sold = 0;
  double revenue = 0.0;
};


int main() {
  Sales_data book;
  Sales_data nextBook;
  double price = 0.0;
  int count = 0;

  if (std::cin >> book.bookNo >> book.units_sold >> price) {
    ++count;

    while (std::cin >> nextBook.bookNo >> nextBook.units_sold >> price) {
      if (book.bookNo == nextBook.bookNo) {
        ++count;
      } else {
        std::cout << book.bookNo << ": " << count << std::endl;
        book = nextBook;
        count = 1;
      }
    }
    std::cout << book.bookNo << ": " << count << std::endl;
  } else {
    std::cout << "No data." << std::endl;
    return -1;
  }

  return 0;
}
